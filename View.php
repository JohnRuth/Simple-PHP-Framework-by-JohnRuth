<?php

class TemplateLoader
{
	/*
	*
	*	Stores template file content
	*
	*/
	private $tmpl;
	
	
	/*
	*
	*	Stores information about template variables names and its values
	*
	*/
	private $assignedValues = array();
	
	
	/*
	*
	*	Description: triggers setTemplate function
	*
	*	Attributes: $path - stores template path
	*
	*/
	public function __construct( $path = '')
	{
		setTemplate($path);
	}
	
	/*
	*
	*	Description: if file pointed out by $path attribute exists, its content is stored in $this->tmpl
	*
	*	Attributes: $path - stores template path
	*
	*/
	public function setTemplate( $path = '')
	{
		if(file_exists($path))
		{
			$this->tmpl = file_get_contents($path);
		}
	}
	
	/*
	*
	*	Description: adds element to assignedValues array
	*
	*	Attributes: 
	*
	*	 $variableName - information about variable name in template
	*
	*	 $variableValue - information about variable value in template
	*/
	public function assignValue($variableName = null, $variableValue = null)
	{
		if(!empty($variableName) && !empty($variableValue))
		{
			$this->assignedValues[strtoupper($variableName)] = $variableValue;
		}
	}
	
	/*
	*
	*	Description: display Template and replace variables with assignedValues
	*
	*/
	public function displayTemplate()
	{
		if(!empty($this->assignedValues))
		{
			foreach($this->assignedValues as $k => $v)
			{
				$this->tmpl = str_replace('{'.$k.'}', $v, $this->tmpl);
			}
		}
		return $this->tmpl;
	}
}

?>