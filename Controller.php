<?php

class Controller
{
	/*
	*	Contains model name as key and its instance as value.
	*/
	protected $models = array();
	
	/*
	*	Contains view name as key and its instance as value.
	*/
	protected $views = array();
	
	
	/*
	*
	*	Description: search for file Model class file and puts its instance in $this->models array
	*	
	*	Attributes: 
	*		$name - name of model class
	*		$path - path that leads to model class file 
	*
	*	Output: true if file was found or false if not
	*
	*/
	public function addModel($name = '', $path = '')
	{
		$fileIncluded = false;
		
		//for full path that points directly to file 
		if(file_exists($path))
		{
			include_once("$path");
			$fileIncluded = true;
		}
		//for path that points to folder containing model with slash in the end
		else if(file_exists($path.$name.'.php'))
		{
			include_once($path.$name.'.php');
			$fileIncluded = true;
		}
		//for path that points to folder containing model without slash in the end
		else if(file_exists("$path/$name.php"))
		{
			include_once("$path/$name.php");
			$fileIncluded = true;
		}
		//for empty path variable and model in same directory as controller
		else if(file_exists("$name.php"))
		{
			include_once("$name.php");
			$fileIncluded = true;
		}
		else
		{
			echo 'tu będzie error handling xd';
		}
		
		if($fileIncluded)
		{
			$this->model[$name] = new $name;
		}
		
		return $fileIncluded;
	}
	
	/*
	*
	*	Description: search for file View class file and puts its instance in $this->views array
	*	
	*	Attributes: 
	*		$name - name of view class
	*		$path - path that leads to view class file 
	*
	*	Output: true if file was found or false if not
	*
	*/
	public function addView($name = '', $path = '')
	{
		$fileIncluded = false;
		
		//for full path that points directly to file 
		if(file_exists($path))
		{
			include_once("$path");
			$fileIncluded = true;
		}
		//for path that points to folder containing view with slash in the end
		else if(file_exists($path.$name.'.php'))
		{
			include_once($path.$name.'.php');
			$fileIncluded = true;
		}
		//for path that points to folder containing view without slash in the end
		else if(file_exists("$path/$name.php"))
		{
			include_once("$path/$name.php");
			$fileIncluded = true;
		}
		//for empty path variable and view in same directory as controller
		else if(file_exists("$name.php"))
		{
			include_once("$name.php");
			$fileIncluded = true;
		}
		else
		{
			echo 'tu będzie error handling xd';
		}
		
		if($fileIncluded)
		{
			$this->view[$name] = new $name;
		}
		
		return $fileIncluded;
	}
}


?>