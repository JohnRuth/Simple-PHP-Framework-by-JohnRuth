<?php

class Model
{
	/*
	*
	*	Description: connects to database using information from config.php file
	*
	*/
	public function connect()
	{
		$this->db = require('config.php');
		
		$this->mysqli = new mysqli($this->db['host'], $this->db['username'],
			$this->db['password'], $this->db['database']);
			
		if ($this->mysqli->connect_errno) 
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/*
	*
	*	Description: disconnects from database
	*
	*/
	public function disconnect()
	{
		$this->mysqli->close();
		return true;
	}
}

?>